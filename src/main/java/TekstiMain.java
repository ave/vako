/**
 * @author hakka
 */

import java.util.Scanner;
import model.*;

public class TekstiMain {
	//static IValuuttaDAO valuuttaDAO = new ValuuttaAccessObjectSQL();
	static IValuuttaDAO valuuttaDAO = new ValuuttaAccessObject(); // Hibisversio
	static Scanner scanner = new Scanner(System.in);

	public static void listaaValuutat() {
		Valuutta[] valuutat = valuuttaDAO.readValuutat();
		for (Valuutta valuutta : valuutat) {
			System.out.println("\t" + valuutta);
		}
	}

	public static void lisääValuutta() {
		String tunnus = lueString("Anna valuutan tunnus: ").toUpperCase();
		if (valuuttaDAO.readValuutta(tunnus) != null) {
			System.out.println("Lisääminen ei onnistu: valuuttatiedot jo olemassa.");
			return;
		}

		double vaihtokurssi = lueDouble("Anna vaihtokurssi (esim. 1.23): ");
		String nimi = lueString("Anna valuutan nimi: ");
		System.out.println(tunnus + ", " + vaihtokurssi + ", " + nimi);

		Valuutta valuutta = new Valuutta(tunnus, vaihtokurssi, nimi);
		if (valuuttaDAO.createValuutta(valuutta) == false) {
			System.out.println("Lisääminen ei onnistunut.");
		}
	}

	public static void päivitäValuutta() {
		String tunnus =	lueString("Anna päivitettävän valuutan tunnus: ");
		Valuutta valuutta = valuuttaDAO.readValuutta(tunnus);
		if (valuutta == null) {
			System.out.println("Antamasi valuutta ei ole tietokannassa");
			return;
		}

		double vaihtokurssi;
		do {
			System.out.println("Nykyinen vaihtokurssi on " + valuutta.getVaihtokurssi());
			vaihtokurssi = lueDouble("Anna uusi vaihtokurssi (esim. 1.23): ");
			if (vaihtokurssi < 0) {
				System.out.println("Negatiivinen arvo ei kelpaa");
			}
		} while (vaihtokurssi < 0);

		valuutta.setVaihtokurssi(vaihtokurssi);
		valuuttaDAO.updateValuutta(valuutta);
	}

	public static void poistaValuutta() {
		String tunnus = lueString("Anna poistettavan valuutan tunnus: ");
		if (valuuttaDAO.deleteValuutta(tunnus) == false) {
			System.out.println("Poistettavaa valuutta ei löydy tietokannasta.");
		}
	}

	// PRIVATE apurutiinit ========================================================/
	private static String lueString(String kehoite) { // paikallinen apurutiini
		System.out.print(kehoite);
		return scanner.nextLine();
	}

	private static double lueDouble(String kehoite) { // paikallinen apurutiini
		double tulos;
		do {
			try {
				System.out.print(kehoite);
				tulos = Double.parseDouble(scanner.nextLine());
				return tulos;
			} catch (Exception e) {
				System.out.println("  ** Virheellinen syöte, anna uudestaan.");
			}
		} while (true);
	}
	// ============================================================================/

	public static void main(String[] args) {
		char valinta;
		final char CREATE = 'C', READ = 'R', UPDATE = 'U', DELETE = 'D', QUIT = 'Q';

		System.out.println("\nValuuttaDB-tietokannan ylläpitorutiinit\n");
		do {
			System.out.println(" C = Lisää uusi valuutta tietokantaan");
			System.out.println(" R = Listaa tietokannassa olevien valuuttojen tiedot");
			System.out.println(" U = Päivitä valuutan vaihtokurssi tietokantaan");
			System.out.println(" D = Poista valuutta tietokannasta");
			System.out.println(" Q = Lopetus");
			System.out.print("Valintasi: ");

			valinta = (scanner.nextLine().toUpperCase()).charAt(0);
			switch (valinta) {
			case CREATE:
				lisääValuutta();
				break;
			case READ:
				listaaValuutat();
				break;
			case UPDATE:
				päivitäValuutta();
				break;
			case DELETE:
				poistaValuutta();
				break;
			}
			;
		} while (valinta != QUIT);
	}
}
