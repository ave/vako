package model;

/**
 * @author hakka
 */

/* 
 * Tällä tasolla voi valita kumpaa mallitason DAO:a käytetään
 * - SQL-kyselyihin perustuvaa vai
 * - Hibernatea käyttävää
 */

public class Valuuttakone implements IValuuttakone {

	private IValuuttaDAO valuuttaDAO = new ValuuttaAccessObject(); // Hibernate
	//private IValuuttaDAO valuuttaDAO = new ValuuttaAccessObjectSQL();
	private Valuutta[] valuutat;

	public String[] getVaihtoehdot() {
		valuutat = valuuttaDAO.readValuutat();
		String[] vaihtoehdot = new String[valuutat.length];
		for (int i = 0; i < valuutat.length; i++) {
			vaihtoehdot[i] = valuutat[i].getNimi();
		}
		return vaihtoehdot;
	}

	public double muunna(int lähtö, int kohde, double määrä) {
		valuutat = valuuttaDAO.readValuutat();
		double arvo = määrä / valuutat[lähtö].getVaihtokurssi();
        return arvo * valuutat[kohde].getVaihtokurssi();
	}
}
