package model;

/**
 * @author Averell
 */

/* Käytetään Hibernate JPA:ta */

import javax.persistence.*;

@Entity
@Table
public class Valuutta {

	@Id
	@Column
	private String tunnus;  // AVAIN

	@Column
	private double vaihtokurssi;

	@Column
	private String nimi; 

	public Valuutta(String tunnus, double vaihtokurssi, String nimi) {
		this.tunnus = tunnus;
		this.vaihtokurssi = vaihtokurssi;
		this.nimi = nimi;
	}

	public Valuutta() {
	}

	public String getTunnus() {
		return tunnus;
	}

	public void setTunnus(String tunnus) {
		this.tunnus = tunnus;
	}

	public double getVaihtokurssi() {
		return vaihtokurssi;
	}

	public void setVaihtokurssi(double vaihtokurssi) {
		this.vaihtokurssi = vaihtokurssi;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	@Override
	public String toString() {
		return tunnus + ", " + nimi + ", " + vaihtokurssi;
	}

}
