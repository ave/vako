package model;

/**
 * @author hakka
 */

/* Tämä DAO käyttää Hibernate JPA:ta */

import org.hibernate.*;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class ValuuttaAccessObject implements IValuuttaDAO {

	private SessionFactory istuntotehdas = null; // on toki oletuksenakin null

	public ValuuttaAccessObject() { // konstruktori
		try {
			istuntotehdas = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.err.println("**** ISTUNTOTEHTAAN LUOMINEN EI ONNISTUNUT " + e.getMessage());
			System.exit(-1);
		}

		/* VANHEMPI TAPA (toimii näinkin):
		 * StandardServiceRegistry registry = new
		 * StandardServiceRegistryBuilder().configure().build();
		 * 
		 * try { istuntotehdas = new
		 * MetadataSources(registry).buildMetadata().buildSessionFactory(); } catch
		 * (Exception e) { System.err.
		 * println("Istuntotehtaan luominen ei onnistunut. Kutsuttu System.exit()");
		 * StandardServiceRegistryBuilder.destroy(registry); e.printStackTrace();
		 * System.exit(-1); }
		 */
	}

	@Override
	public void finalize() { // destruktori
		try {
			if (istuntotehdas != null)
				istuntotehdas.close(); // sulkee automaattisesti muutkin avatut resurssit
		} catch (Exception e) {
			System.err.println("Istuntotehtaan sulkeminen epäonnistui. " + e.getMessage());
		}
	}

	/* === C R E A T E === */

	public boolean createValuutta(Valuutta valuutta) {

		if (readValuutta(valuutta.getTunnus()) != null) {
			return false; // jos on jo olemassa, ei luoda uudestaan
		}

		Transaction transaktio = null;

		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			istunto.saveOrUpdate(valuutta);
			transaktio.commit();
		} catch (Exception e) {
			if (transaktio != null)
				transaktio.rollback();
			e.printStackTrace();
			return false;
		}

		return true; // Kaikki onnistui
	}

	/* === R E A D === */
	/*
	 * 1. session.load() It will always return a “proxy” (Hibernate term) without
	 * hitting the database. In Hibernate, proxy is an object with the given
	 * identifier value, its properties are not initialized yet, it just look like a
	 * temporary fake object. If no row found, it throws an ObjectNotFoundException.
	 *
	 * 2. session.get() It always hit the database and return the real object, an
	 * object that represent the database row, not proxy. If no row found, it return
	 * null.
	 */

	public Valuutta readValuutta(String tunnus) {
		Valuutta valuutta = null; // palauta null, jos ei löydy tietokannasta

		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			valuutta = istunto.get(Valuutta.class, tunnus);
			istunto.getTransaction().commit();
		} catch (ObjectNotFoundException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return valuutta;
	}

	@SuppressWarnings("unchecked")
	public Valuutta[] readValuutat() {

		List<Valuutta> valuutat = null;
		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			valuutat = istunto.createQuery("from Valuutta").getResultList();
			istunto.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Valuutta[] returnArray = new Valuutta[valuutat.size()];
		return (Valuutta[]) valuutat.toArray(returnArray);
	}

	/* === U P D A T E === */

	public boolean updateValuutta(Valuutta valuutta) {
		Transaction transaktio = null;
		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			// päivitykset Valuutta-olioon tehty jo ennen kutsua
			istunto.saveOrUpdate(valuutta);
			istunto.getTransaction().commit();
			return true;
		} catch (Exception e) {
			if (transaktio != null)
				transaktio.rollback();
		}
		return false; // tähän vain poikkeuksen jälkeen
	}

	/* === D E L E T E === */

	public boolean deleteValuutta(String tunnus) {

		Transaction transaktio = null;
		Valuutta valuutta = new Valuutta();

		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			istunto.load(valuutta, tunnus);
			istunto.delete(valuutta);
			transaktio.commit();
		} catch (ObjectNotFoundException e) {
			return false;
		} catch (Exception e) {
			return false;
		}

		return true; // tähän vain ellei poikkeusta
	}
}
