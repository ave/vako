package view;

/**
 * @author hakka
 */

import model.Valuuttakone;
import model.IValuuttakone;
import controller.*;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class ValuuttakoneenGUI extends Application implements IValuuttakoneenGUI {
	private IValuuttakoneenOhjain kontrolleri;

	private TextField määrä;
	private TextField muunnos;
	private String[] valuuttataulukko;
	private ListView<String> valuuttalista;
	private ListView<String> valuuttalista2;

	@Override
	public void init() {
		// MVC-rakenteen luonti
		IValuuttakone model = new Valuuttakone();
		kontrolleri = new ValuuttakoneenOhjain(this, model);
		// Hae valuuttojen nimet tietokannasta
		valuuttataulukko = kontrolleri.getValuutat();  
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Valuuttakone");
			HBox root = createHBox();
			Scene scene = new Scene(root);

			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private HBox createHBox() {
		// Perusrakenteena vaakasuuntainen boxi
		HBox hBox = new HBox();
		hBox.setPadding(new Insets(15, 12, 15, 12)); // ylä, oikea, ala, vasen
		hBox.setSpacing(10); // väleihin 10 pikseliä

		// Listan vaihtoehdot
		ObservableList<String> valuutat = FXCollections.observableArrayList();
		for (int i = 0; i < valuuttataulukko.length; i++) {
			valuutat.add(valuuttataulukko[i]);
		}

		// Vasemmanpuoleinen lista
		valuuttalista = new ListView<String>();
		valuuttalista.setPrefSize(180, 120);
		valuuttalista.setItems(valuutat);

		// Oikeanpuoleinen lista
		valuuttalista2 = new ListView<String>();
		valuuttalista2.setPrefSize(180, 120);
		valuuttalista2.setItems(valuutat);

		// Otsikko ja lista pystysuuntaiseen boxiin, molemmille listoille oma
		VBox valuuttaBox1 = new VBox();
		valuuttaBox1.getChildren().addAll(new Label("Mistä"), valuuttalista);
		VBox valuuttaBox2 = new VBox();
		valuuttaBox2.getChildren().addAll(new Label("Mihin"), valuuttalista2);

		// Oikean reunan pystysuuntainen boxi
		VBox määräBox = new VBox();
		määrä = new TextField();
		määrä.setPrefSize(100, 20);
		määräBox.setSpacing(5); // osien välimatka 5 pikseliä
		määräBox.getChildren().addAll(new Label("Määrä"), määrä);

		Button buttonMuunna = new Button("Muunna");
		buttonMuunna.setPrefSize(100, 20);
		määräBox.getChildren().addAll(buttonMuunna);

		muunnos = new TextField();
		muunnos.setPrefSize(100, 20);

		final Text error = new Text("Virheellinen syöte!");
		error.setFill(Color.RED);
		error.setVisible(false); // Virheilmoitus ei näkyvissä
		määräBox.getChildren().addAll(new Label("Tulos"), muunnos, error);

		// BUTTON ACTION
		buttonMuunna.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				try {
					if (getMäärä() > 0) {
						kontrolleri.muunnos();
					}
				} catch (Exception e2) {
					// Näytä virheilmoitus, ja poista se näkyvistä kolmen sekunnin kuluttua
					error.setVisible(true);
					FadeTransition fadeOut = new FadeTransition(Duration.millis(3000));
					fadeOut.setNode(error);
					fadeOut.setFromValue(1.0);
					fadeOut.setToValue(0.0);
					fadeOut.setCycleCount(1);
					fadeOut.setAutoReverse(false);
					fadeOut.playFromStart();

					// Toinen tapa: näytä virheilmoitus Alert-ikkunassa
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Valuuttakone");
					alert.setHeaderText(null);
					alert.setContentText("Vaihdettavan valuuttamäärän tulee olla numeerinen.");

					alert.showAndWait();
				}
			}
		});

		// MAINBOX
		// Täytä pääboxi aiemmin tehdyillä boxeilla ja tekstikentillä.
		hBox.getChildren().addAll(valuuttaBox1, valuuttaBox2, määräBox);
		return hBox;
	}

	// GUI:n metodit
	// ------------------------------------------------------------------------------------------------------
	public double getMäärä() {
		return Double.parseDouble(määrä.getText());
	}

	public int getLähtöIndeksi() {
		return valuuttalista.getSelectionModel().getSelectedIndex();
	}

	public int getKohdeIndeksi() {
		return valuuttalista2.getSelectionModel().getSelectedIndex();
	}

	public void setTulos(double tulos) {
		// Näytä kahdella desimaalilla
		muunnos.setText(String.format("%.2f",tulos));
	}
	// ------------------------------------------------------------------------------------------------------

	public static void main(String[] args) {
		launch(args);
	}
}