package controller;

/**
 * @author hakka
 */

import model.*;
import view.*;

public class ValuuttakoneenOhjain implements IValuuttakoneenOhjain {
	private IValuuttakone model;
	private IValuuttakoneenGUI gui;

	public ValuuttakoneenOhjain(IValuuttakoneenGUI gui, IValuuttakone model) {
		this.gui = gui;
		this.model = model;
	}

    public String[] getValuutat() {
		return model.getVaihtoehdot();
	}


	public void muunnos() {
		double määrä = model.muunna(gui.getLähtöIndeksi(), gui.getKohdeIndeksi(), gui.getMäärä());
		gui.setTulos(määrä);
	}
}
